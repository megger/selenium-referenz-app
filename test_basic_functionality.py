# -*- coding: utf-8 -*-
# imports unittest to do the tests
import unittest
# imports qfqselenium which contains the custom qfq tests
import qfqselenium


class TestBasicFunctionality(qfqselenium.QfqSeleniumTestCase):
    """
    this class contains the tests to test the implementation of qfq itself.
    """
    # stores the path including the index file of the form to test with
    basic_form_path = "index.php?id=basicform"

    # stores the data reference of the add data button
    add_button_data_ref = "addBasicData"
    # stores the data reference of the delete data button
    delete_button_data_ref = "deleteBasicData"
    # stores the data reference of the edit data button
    edit_button_data_ref = "editBasicData"

    # stores the data reference for the text field
    text_ref = "text"
    # stores the data reference for the number field
    number_ref = "number"
    # stores the data reference for the date field
    date_ref = "date"
    # stores the data reference for the datetime field
    datetime_ref = "datetime"
    # stores the data reference for the decimal field
    decimal_ref = "decimal"
    # stores the data reference for the dropdown field
    dropdown_ref = "enum"
    # stores the data reference for the dynamic update field
    dynamic_update_ref = "dynamicUpdate"
    # stores the data reference for the pill_text field
    pill_text_ref = "pill_text"
    # stores the data reference for the file upload
    file_upload_ref = "file"
    # stores the name of the first pill
    pill1_text = "pill1"
    # stores the name of the second pill
    pill2_text = "pill2"

    # stores the default date
    default_date = "01.01.2188"
    # stores the default datetime
    default_datetime = "02.02.2188 01:08:39"
    # stores the default decimal number
    default_decimal = "12.84"
    # stores the default dropdown option
    default_dropdown = "first option"
    # stores the default radio button
    default_radio = "option a"
    # stores the dynamic update option for the radio buttons
    dynamic_update_radio = "option c"
    # stores the default checkbox
    default_checkbox = "2"


    def _fill_in_default_data(self, unique_text, unique_number):
        """
        this function fills in the default values into the form
        and the given unique parts
        """
        s = self

        # fills the unique text into the text field
        s.qfq_fill_textfield(s.text_ref, unique_text)
        # fills the unique number into the number field
        s.qfq_fill_textfield(s.number_ref, unique_number)
        # fills the default date into the date field
        s.qfq_fill_textfield(s.date_ref, s.default_date)
        # fills the default datetime into the datetime field
        s.qfq_fill_textfield(s.datetime_ref, s.default_datetime)
        # fills the default decimal into the decimal field
        s.qfq_fill_textfield(s.decimal_ref, s.default_decimal)
        # selects the default option from the enum dropdown
        s.qfq_dropdown_select(s.dropdown_ref, s.default_dropdown)
        # selects the default option from the radio buttons
        s.qfq_radio_select(s.default_radio)
        # selects the default option from the checkboxes
        s.qfq_checkbox_select(s.default_checkbox)


    def test_qfq_canary(self):
        """
        this test should always succeed because it asserts if 1 equals 1.
        if it doen't succeed there is probably something wrong with
        the environment.
        """
        s = self

        # defines an always true assert that checks if 1 equals 1
        s.qfq_assert_true(1 == 1) # NOTE if this test fails, something is wrong

    def test_qfq_create_delete(self):
        """
        this test checks if a new data entry can be created and deleted again.
        """
        s = self

        # ----- UNIQUE IDENTIFIERS ----- #

        # generates a random unique text
        unique_text = s.qfq_generate_random_string()
        # generates a random unique number
        unique_number = str(s.qfq_generate_random_number())

        # ----- CREATE ----- #

        # goes to the basic form page
        s.qfq_goto_page(s.basic_form_path)

        # clicks the plus button to create a new data entry
        s.qfq_click_element_with_data_ref(s.add_button_data_ref)

        # fills in the defaults and the unique parts
        s._fill_in_default_data(unique_text, unique_number)

        # clicks the save button to save the data
        s.qfq_click_save_form_button()
        # clicks the close button to return to the list of data entries
        s.qfq_click_close_form_button()

        # ----- ASSERTS ----- #

        # checks if the unique text exists on the page
        s.qfq_assert_text_exists(unique_text)
        # checks if the unique number exists on the page
        s.qfq_assert_text_exists(unique_number)

        # ----- DELETE ----- #

        # clicks the delete button to delete the before created data entry
        s.qfq_click_element_with_data_ref(s.delete_button_data_ref + unique_number)
        # confirms the deletion of the data
        s.qfq_click_element_with_text("button", "Ok")

        # ----- ASSERTS ----- #

        # checks if there is no unique text anymore after deleting it
        s.qfq_assert_text_absent(unique_text)
        # checks if there is no unique number anymore
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_create_edit_delete(self):
        """
        this test checks if a data entry can be created, edited and
        deleted again.
        """
        s = self

        # ----- UNIQUE IDENTIFIERS ----- #

        # generates a random unique text
        unique_text = s.qfq_generate_random_string()
        # generates a random unique number
        unique_number = str(s.qfq_generate_random_number())

        # ----- CREATE ----- #

        # goes to the basic form page
        s.qfq_goto_page(s.basic_form_path)

        # clicks the plus button to create a new data entry
        s.qfq_click_element_with_data_ref(s.add_button_data_ref)

        # fills in the defaults and the unique parts
        s._fill_in_default_data(unique_text, unique_number)

        # clicks the save button to save the data
        s.qfq_click_save_form_button()
        # clicks the close button to return to the list of data entries
        s.qfq_click_close_form_button()

        # ----- ASSERTS ----- #

        # checks if the unique text exists on the page
        s.qfq_assert_text_exists(unique_text)
        # checks if the unique number exists on the page
        s.qfq_assert_text_exists(unique_number)

        # ----- EDIT ----- #

        # generates a new random unique text
        new_unique_text = s.qfq_generate_random_string()
        # generates a new random unique number
        new_unique_number = str(s.qfq_generate_random_number())

        # clicks the edit button to edit the before created data entry
        s.qfq_click_element_with_data_ref(self.edit_button_data_ref + unique_number)

        # checks if the unique text exists on the edit form page
        s.qfq_assert_text_exists(unique_text)
        # checks if the unique number exists on the edit form page
        s.qfq_assert_text_exists(unique_number)

        # clears the text field
        s.qfq_clear_textfield(s.text_ref)
        # fills the new unique text into the empty text field
        s.qfq_fill_textfield(s.text_ref, new_unique_text)
        # clears the number field
        s.qfq_clear_textfield(s.number_ref)
        # fills the new unique number into the empty number field
        s.qfq_fill_textfield(s.number_ref, new_unique_number)

        # clicks the save button to save the data
        s.qfq_click_save_form_button()
        # clicks the close button to return to the list of data entries
        s.qfq_click_close_form_button()

        # ----- ASSERTS ----- #

        # checks if the new unique text exists on the page
        s.qfq_assert_text_exists(new_unique_text)
        # checks if the new unique number exists on the page
        s.qfq_assert_text_exists(new_unique_number)

        # checks if there is no old unique text anymore after editing it
        s.qfq_assert_text_absent(unique_text)
        # checks if there is no old unique number anymore
        s.qfq_assert_text_absent(unique_number)

        # ----- DELETE ----- #

        # clicks the delete button to delete the before created data entry
        s.qfq_click_element_with_data_ref(s.delete_button_data_ref + new_unique_number)
        # confirms the deletion of the data
        s.qfq_click_element_with_text("button", "Ok")

        # ----- ASSERTS ----- #

        # checks if there is no new unique text anymore after deleting it
        s.qfq_assert_text_absent(new_unique_text)
        # checks if there is no new unique number anymore
        s.qfq_assert_text_absent(new_unique_number)

    def test_qfq_create_incomplete(self):
        """
        this test checks that an incomplete form cannot be submitted and
        saved by trying to save it directly and by trying to save it on
        the 'save before closing' prompt.
        """
        s = self

        # ----- UNIQUE IDENTIFIERS ----- #

        # generates a random unique text
        unique_text = s.qfq_generate_random_string()
        # generates a random unique number
        unique_number = str(s.qfq_generate_random_number())

        # ----- TRY TO CREATE ----- #

        # goes to the basic form page
        s.qfq_goto_page(s.basic_form_path)

        # clicks the plus button to create a new data entry
        s.qfq_click_element_with_data_ref(s.add_button_data_ref)

        # fills the unique text into the text field
        s.qfq_fill_textfield(s.text_ref, unique_text)
        # fills the unique number into the number field
        s.qfq_fill_textfield(s.number_ref, unique_number)

        # clicks the save button to try to save the incomplete data
        s.qfq_click_save_form_button()

        # clicks the close button to close the form
        s.qfq_click_close_form_button()
        # waits 1 second
        s.qfq_wait(1) # NOTE window freezes if the button is clicked to fast
        # clicks the cancel button to cancel the closing
        s.qfq_click_element_with_text("button", "Cancel")

        # clicks the close button to close the form
        s.qfq_click_close_form_button()
        # waits 1 second
        s.qfq_wait(1) # NOTE window freezes if the button is clicked to fast
        # clicks the yes button to tell the app to save the incomplete data
        s.qfq_click_element_with_text("button", "Yes")

        # clicks the close button to close the form
        s.qfq_click_close_form_button()
        # waits 1 second
        s.qfq_wait(1) # NOTE window freezes if the button is clicked to fast
        # clicks the no button to close wuithout saving
        s.qfq_click_element_with_text("button", "No")

        # ----- ASSERTS ----- #

        # checks if there is no unique text because it was never saved
        s.qfq_assert_text_absent(unique_text)
        # checks if there is no unique number
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_close_empty_form(self):
        """
        this test checks if an never edited new form can be closed
        without confirming.
        """
        s = self

        # ----- CREATE FROM ----- #

        # goes to the basic form page
        s.qfq_goto_page(s.basic_form_path)

        # clicks the plus button to create a new data entry
        s.qfq_click_element_with_data_ref(s.add_button_data_ref)

        # NOTE doesn't fill in data

        # ----- CLOSE FORM ----- #

        # clicks the close button to close the empty form
        s.qfq_click_close_form_button()

        # ----- ASSERTS ----- #

        # asserts if the page url doesn't contain '&s=' (e.g. is the home page)
        s.qfq_assert_true('&s=' not in s.qfq_get_url())

    def test_qfq_create_form_with_pill_text_delete(self):
        """
        this test creates a form with the additional pill_text from pill2
        """
        s = self

        # ----- UNIQUE IDENTIFIERS ----- #

        # generates a random unique text
        unique_text = s.qfq_generate_random_string()
        # generates a random unique text for use in pill_text
        unique_pill_text = s.qfq_generate_random_string()
        # generates a random unique number
        unique_number = str(s.qfq_generate_random_number())

        # ----- CREATE ----- #

        # goes to the basic form page
        s.qfq_goto_page(s.basic_form_path)

        # clicks the plus button to create a new data entry
        s.qfq_click_element_with_data_ref(s.add_button_data_ref)

        # fills in the defaults and the unique parts
        s._fill_in_default_data(unique_text, unique_number)

        # switches to pill2
        s.qfq_open_pill(s.pill2_text)
        # fills the unique pill_text into the pill text field
        s.qfq_fill_textfield(s.pill_text_ref, unique_pill_text)

        # clicks the save button to save the data
        s.qfq_click_save_form_button()
        # clicks the close button to return to the list of data entries
        s.qfq_click_close_form_button()

        # ----- ASSERTS ----- #

        # checks if the unique text exists on the page
        s.qfq_assert_text_exists(unique_text)
        # checks if the unique number exists on the page
        s.qfq_assert_text_exists(unique_number)
        # checks if the unique pill text exists on the page
        s.qfq_assert_text_exists(unique_pill_text)

        # ----- DELETE ----- #

        # clicks the delete button to delete the before created data entry
        s.qfq_click_element_with_data_ref(s.delete_button_data_ref + unique_number)
        # confirms the deletion of the data
        s.qfq_click_element_with_text("button", "Ok")

        # ----- ASSERTS ----- #

        # checks if there is no unique text anymore after deleting it
        s.qfq_assert_text_absent(unique_text)
        # checks if there is no unique number anymore
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_valid_file_upload(self):
        """
        this test checks if the fileupload works
        """
        s = self

        # ----- UNIQUE IDENTIFIERS ----- #

        # generates a random unique text
        unique_text = s.qfq_generate_random_string()
        # generates a random unique number
        unique_number = str(s.qfq_generate_random_number())
        # generates a unique file_name
        unique_file_name = s.qfq_generate_random_string(8)

        # ----- CREATE ----- #

        # goes to the basic form page
        s.qfq_goto_page(s.basic_form_path)

        # clicks the plus button to create a new data entry
        s.qfq_click_element_with_data_ref(s.add_button_data_ref)

        # fills in the defaults and the unique parts
        s._fill_in_default_data(unique_text, unique_number)

        # switches to pill2
        s.qfq_open_pill(s.pill2_text)
        # uploads the file
        s.qfq_upload_file(s.file_upload_ref, unique_file_name, "txt", 32)

        # clicks the save button to save the data
        s.qfq_click_save_form_button()
        # clicks the close button to return to the list of data entries
        s.qfq_click_close_form_button()

        # ----- ASSERTS ----- #

        # checks if the unique text exists on the page
        s.qfq_assert_text_exists(unique_text)
        # checks if the unique number exists on the page
        s.qfq_assert_text_exists(unique_number)
        # checks if the unique file name exists on the page
        s.qfq_assert_text_exists(unique_file_name)

        # ----- DELETE ----- #

        # clicks the delete button to delete the before created data entry
        s.qfq_click_element_with_data_ref(s.delete_button_data_ref + unique_number)
        # confirms the deletion of the data
        s.qfq_click_element_with_text("button", "Ok")

        # ----- ASSERTS ----- #

        # checks if there is no unique text anymore after deleting it
        s.qfq_assert_text_absent(unique_text)
        # checks if there is no unique number anymore
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_wrong_file_type_upload(self):
        """
        this test checks if a wrong filetype throws
        an error when beeing uploaded
        """
        s = self

        # ----- UNIQUE IDENTIFIERS ----- #

        # generates a random unique text
        unique_text = s.qfq_generate_random_string()
        # generates a random unique number
        unique_number = str(s.qfq_generate_random_number())
        # generates a unique file_name
        unique_file_name = s.qfq_generate_random_string(8)

        # ----- CREATE ----- #

        # goes to the basic form page
        s.qfq_goto_page(s.basic_form_path)

        # clicks the plus button to create a new data entry
        s.qfq_click_element_with_data_ref(s.add_button_data_ref)

        # fills in the defaults and the unique parts
        s._fill_in_default_data(unique_text, unique_number)

        # switches to pill2
        s.qfq_open_pill(s.pill2_text)
        # uploads the file
        s.qfq_upload_file(s.file_upload_ref, unique_file_name, "rtf", 32)

        # waits 1 second
        s.qfq_wait(1) # NOTE window freezes if the button is clicked to fast
        # clicks the ok button to close the error notification
        s.qfq_click_element_with_text("button", "Ok")
        # waits 1 second
        s.qfq_wait(1) # NOTE window freezes if the button is clicked to fast

        # clicks the save button to save the data
        s.qfq_click_save_form_button()
        # clicks the close button to return to the list of data entries
        s.qfq_click_close_form_button()

        # ----- ASSERTS ----- #

        # checks if the unique text exists on the page
        s.qfq_assert_text_exists(unique_text)
        # checks if the unique number exists on the page
        s.qfq_assert_text_exists(unique_number)
        # checks if the unique file name is absent on the page
        s.qfq_assert_text_absent(unique_file_name)

        # ----- DELETE ----- #

        # clicks the delete button to delete the before created data entry
        s.qfq_click_element_with_data_ref(s.delete_button_data_ref + unique_number)
        # confirms the deletion of the data
        s.qfq_click_element_with_text("button", "Ok")

        # ----- ASSERTS ----- #

        # checks if there is no unique text anymore after deleting it
        s.qfq_assert_text_absent(unique_text)
        # checks if there is no unique number anymore
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_wrong_to_large_file_upload(self):
        """
        this test checks if a file that is too large throws
        an error when beeing uploaded
        """
        s = self

        # ----- UNIQUE IDENTIFIERS ----- #

        # generates a random unique text
        unique_text = s.qfq_generate_random_string()
        # generates a random unique number
        unique_number = str(s.qfq_generate_random_number())
        # generates a unique file_name
        unique_file_name = s.qfq_generate_random_string(8)

        # ----- CREATE ----- #

        # goes to the basic form page
        s.qfq_goto_page(s.basic_form_path)

        # clicks the plus button to create a new data entry
        s.qfq_click_element_with_data_ref(s.add_button_data_ref)

        # fills in the defaults and the unique parts
        s._fill_in_default_data(unique_text, unique_number)

        # switches to pill2
        s.qfq_open_pill(s.pill2_text)
        # uploads the file that is too large (128b, 64b max)
        s.qfq_upload_file(s.file_upload_ref, unique_file_name, "txt", 128)

        # waits 1 second
        s.qfq_wait(1) # NOTE window freezes if the button is clicked to fast
        # clicks the ok button to close the error notification
        s.qfq_click_element_with_text("button", "Ok")
        # waits 1 second
        s.qfq_wait(1) # NOTE window freezes if the button is clicked to fast

        # clicks the save button to save the data
        s.qfq_click_save_form_button()
        # clicks the close button to return to the list of data entries
        s.qfq_click_close_form_button()

        # ----- ASSERTS ----- #

        # checks if the unique text exists on the page
        s.qfq_assert_text_exists(unique_text)
        # checks if the unique number exists on the page
        s.qfq_assert_text_exists(unique_number)
        # checks if the unique file name is absent on the page
        s.qfq_assert_text_absent(unique_file_name)

        # ----- DELETE ----- #

        # clicks the delete button to delete the before created data entry
        s.qfq_click_element_with_data_ref(s.delete_button_data_ref + unique_number)
        # confirms the deletion of the data
        s.qfq_click_element_with_text("button", "Ok")

        # ----- ASSERTS ----- #

        # checks if there is no unique text anymore after deleting it
        s.qfq_assert_text_absent(unique_text)
        # checks if there is no unique number anymore
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_dynamic_update(self):
        """
        this test checks if the dynamic update field
        in the form gets saved correctly
        """
        s = self

        # ----- UNIQUE IDENTIFIERS ----- #

        # generates a random unique text
        unique_text = s.qfq_generate_random_string()
        # generates a random unique text for the dynamic update field
        unique_dynamic_update_text = s.qfq_generate_random_string()
        # generates a random unique number
        unique_number = str(s.qfq_generate_random_number())

        # ----- CREATE ----- #

        # goes to the basic form page
        s.qfq_goto_page(s.basic_form_path)

        # clicks the plus button to create a new data entry
        s.qfq_click_element_with_data_ref(s.add_button_data_ref)

        # fills in the defaults and the unique parts
        s._fill_in_default_data(unique_text, unique_number)

        # checks if the dynamic text field is hidden
        s.qfq_assert_false(
            s.qfq_element_is_visible(
                s.qfq_get_element_by_data_ref(s.dynamic_update_ref)))
        # selects the dynamic update option from the radio buttons
        s.qfq_radio_select(s.dynamic_update_radio)
        # checks if the dynamic text field is visible
        s.qfq_assert_true(
            s.qfq_element_is_visible(
                s.qfq_get_element_by_data_ref(s.dynamic_update_ref)))
        # fills in the unique dynamic update text into the dynamic update field
        s.qfq_fill_textfield(s.dynamic_update_ref, unique_dynamic_update_text)

        # clicks the save button to save the data
        s.qfq_click_save_form_button()
        # clicks the close button to return to the list of data entries
        s.qfq_click_close_form_button()

        # ----- ASSERTS ----- #

        # checks if the unique text exists on the page
        s.qfq_assert_text_exists(unique_text)
        # checks if the unique number exists on the page
        s.qfq_assert_text_exists(unique_number)
        # checks if the unique dynamic update text exists on the page
        s.qfq_assert_text_exists(unique_dynamic_update_text)

        # ----- DELETE ----- #

        # clicks the delete button to delete the before created data entry
        s.qfq_click_element_with_data_ref(s.delete_button_data_ref + unique_number)
        # confirms the deletion of the data
        s.qfq_click_element_with_text("button", "Ok")

        # ----- ASSERTS ----- #

        # checks if there is no unique text anymore after deleting it
        s.qfq_assert_text_absent(unique_text)
        # checks if there is no unique number anymore
        s.qfq_assert_text_absent(unique_number)

if __name__ == "__main__":
    unittest.main()
