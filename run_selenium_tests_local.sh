#!/bin/bash

# this function prints a separator
function print_separator {
  # prints the separator
  echo -e "----------------------------------------------------------------------"
}

# prints the starting separator
print_separator

# checks that a file named geckodriver doesn't already exist
if [ ! -f "geckodriver" ]; then
  # stores the current version of the driver
  gecko_version=$(curl --silent "https://api.github.com/repos/mozilla/geckodriver/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")') &> /dev/null
  # downloads the geckodriver from github
  wget -O /tmp/geckodriver.tar.gz https://github.com/mozilla/geckodriver/releases/download/${gecko_version}/geckodriver-${gecko_version}-linux64.tar.gz &> /dev/null
  # unzips the downloaded geckodriver
  tar xzf /tmp/geckodriver.tar.gz geckodriver &> /dev/null
  # makes the geckodriver executable
  chmod +x geckodriver &> /dev/null
  # prints a success output
  echo -e "Successfully downloaded geckodriver"
fi

# checks that a file named chromedriver doesn't already exist
if [ ! -f "chromedriver" ]; then
  # downloads the newest version of the chromedriver from google
  wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip &> /dev/null
  # unzips the downloaded chromedriver
  unzip /tmp/chromedriver.zip chromedriver &> /dev/null
  # makes the chromedriver executable
  chmod +x chromedriver &> /dev/null
  # prints a success output
  echo -e "Successfully downloaded chromedriver"
fi

# reads the url to test from the input variable
SELENIUM_URL=$1

# checks if the selenium url is not given
if [ -z $SELENIUM_URL ]; then
  # prints the error message that no url was given
  echo -e "Error: No testing url was given"
  # exits execution with an undefined error
  exit 1
fi

# defines the url to be used during testing
export SELENIUM_URL=$SELENIUM_URL

# stores the default engine
DEFAULT_ENGINE="chrome"
# reads the engine from the 2nd input variable
ENGINE=$2

# checks if an engine is not specified
if [ -z $ENGINE ]; then
  # defines the default engine to use during tests
  export BROWSER=$DEFAULT_ENGINE
  # defines the path to the drivers of the engine
  export DRIVER_PATH="${PWD}/${DEFAULT_ENGINE}driver"
else
  # defines the engine to use during tests
  export BROWSER=$ENGINE
  # defines the path to the drivers of the engine
  export DRIVER_PATH="${PWD}/${ENGINE}driver"
fi

# stores the default headless option
DEFAULT_HEADLESS="yes"
# reads the headless option from the 3rd input variable
HEADLESS=$3

# checks if the headless parameter is not specified
if [ -z $HEADLESS ]; then
  # defines if the browser gui should open
  export SELENIUM_HEADLESS=$DEFAULT_HEADLESS
else
  # defines if the browser gui should open
  export SELENIUM_HEADLESS=$HEADLESS
fi

# stores the default slowdown
DEFAULT_SLOWDOWN="0"
# reads the slowdown from the 4th input variable
SLOWDOWN=$4

# checks if the slowdown parameter is not specified
if [ -z $SLOWDOWN ]; then
  # defines what slowdown should be applied
  export SELENIUM_SLOWDOWN=$DEFAULT_SLOWDOWN
else
  # defines what slowdown should be applied
  export SELENIUM_SLOWDOWN=$SLOWDOWN
fi

# prints running tests
echo -e -n "Running tests: "

# runs the python tests
python3 -W ignore -m unittest discover

# checks if the tests were successful
if [ $? -eq 0 ]; then
  print_separator
  # prints a success message
  echo -e "Successfully tested ${SELENIUM_URL}"
else
  print_separator
  # prints a success message
  echo -e "Failed while testing ${SELENIUM_URL}"
fi

# prints the trailing separator
print_separator

# exits the program
exit 0
